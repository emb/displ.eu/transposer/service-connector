# Service Connector

Connect a process, abstracted in an *Adapter* to the *Transposer Pipeline*

## Setup

### Get Repository

```bash
cd /your/target/dir
git clone https://git.fairkom.net/emb/displ.eu/transposer/service-connector.git
cd service-connector
```

### Create and Activate `venv`

If using vanilla Python
```bash
python -m venv ./venv
source venv/bin/activate
```

If using Anaconda
```bash
conda config --add channels conda-forge
conda create -n <environment-name> --file requirements.conda.txt
conda activate <environment-name>
```

### Install Dependencies

If using vanilla Python
```bash
pip install -r requirements.txt
```


### Run Application

```bash
./app/connector
```

