# import the baseclass for the adapter
# this class loads parses the config
# file at config_path
import base_adapter

# import the modules you need for your
# adapter implementation
import sys
import os
import typing

import time
import random
import logging


class Adapter(base_adapter.BaseAdapter):
    # You can define your own slots here.
    slots = [
    
    ]
    
    # you can omit __init__ altogether,
    # but if you use it, follow this syntax
    # and call init in the base class to
    # load and parse the adapter config.
    # a reference to the connector will be
    # available at 'self.connector'
    # the adapter's config will be available at
    # self.config
    def __init__(self, connector: typing.Any, config_path: str | None = None) -> None:
        super(Adapter, self).__init__(connector, config_path)
        
        print('BASH Adapter ... config:', self.config)
        
        #
        # Initialize your adapter here
        #
    
    
    def run(self, payload, msg):
        print('This is the BASH Adapter')
        
        return {
            'success': True
        }


    def adapter_name(self):
        return 'bash'

