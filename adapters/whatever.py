# import the baseclass for the adapter
# this class loads parses the config
# file at config_path
import base_adapter

# import the modules you need for your
# adapter implementation
import sys
import os
import typing

import time
import random
import logging


class Adapter(base_adapter.BaseAdapter):
    # You can define your own slots here.
    slots = [
    
    ]
    
    # you can omit __init__ altogether,
    # but if you use it, follow this syntax
    # and call init in the base class to
    # load and parse the adapter's config.
    # a reference to the connector will be
    # available at 'self.connector'
    # the adapter's config will be available at
    # self.config
    def __init__(self, connector: typing.Any, config_path: str | None = None) -> None:
        super(Adapter, self).__init__(connector, config_path)
        
        print('MOCK Adapter ... config:', self.config)
        print('MOCK Adapter ... config.sections:', self.config.sections)
        
        for key, val in self.config:
            print('MOCK Adapter ... config iter:', val.name, '=', val.options)
            for k, v in val:
                print('    section iter:', k, '=', v)
        
        #
        # Initialize your adapter here
        #
    
    
    def run(self, payload, msg):
        print('This is the MOCK Adapter')
        print('MOCK Adapter ... run ... payload, msg:', payload, msg)
        
        # decide how long this process will take
        sleep_dur = 0.2 + (random.random() * 4)
        
        print(f'MOCK Adapter ... run ... now waiting for {sleep_dur} secs.')
        
        time.sleep(sleep_dur)
        
        # decide the state of this process
        success = random.random() < 0.5
        success_str = 'success' if success else 'failed'
        
        print(f'MOCK Adapter ... run ... state of this process: {success_str}')
        
        # create result object
        result = {
            'success': success
        }
        if not success:
            result['message'] = 'Some error occurred'
        
        # return result
        return result


    def adapter_name(self):
        return 'whatever'

