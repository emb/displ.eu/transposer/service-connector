# Adapter Base Class
# Its main purpose is to load and parse
# the config given by 'config_path'.
# This class is part of the transposer project
# and follows its licensing.

import os
import typing
import json

from deepmerge import always_merger

from trp_config import TrpConfig
from service_connector import ServiceConnector


class BaseAdapter:
    __slots__ = [
        'connector',
        'config'
    ]
    
    def __init__(self, connector: ServiceConnector, config_path: str | None = None) -> None:
        self.connector = connector
        
        # Load adapter config
        adapter_config_files = []
        
        # prepare file names
        a_name = self.adapter_name()
        p_ini = f'{a_name}.ini'
        p_def_ini = f'{a_name}.defaults.ini'
        
        # add config files residing in adapter dirs
        for d in self.connector.config.get('appdirs').get('adapters_dirs'):
            p = os.path.join(d, p_def_ini)
            if os.path.isfile(p):
                adapter_config_files.append(p)
            
            p = os.path.join(d, p_ini)
            if os.path.isfile(p):
                adapter_config_files.append(p)
        
        # add config files residing in config dirs
        p = os.path.join(
            self.connector.config.get('appdirs').get('adapters_config'),
            p_def_ini
        )
        if os.path.isfile(p):
            adapter_config_files.append(p)
        
        p = os.path.join(
            self.connector.config.get('appdirs').get('adapters_config'),
            p_ini
        )
        if os.path.isfile(p):
            adapter_config_files.append(p)

        p = f'/etc/transposer/adapters/{a_name}.defaults.ini'
        if os.path.isfile(p):
            adapter_config_files.append(p)

        p = f'/etc/transposer/adapters/{a_name}.ini'
        if os.path.isfile(p):
            adapter_config_files.append(p)
        
        p = os.path.expanduser(f'~/.config/transposer/adapters/{a_name}.defaults.ini')
        if os.path.isfile(p):
            adapter_config_files.append(p)
        
        p = os.path.expanduser(f'~/.config/transposer/adapters/{a_name}.ini')
        if os.path.isfile(p):
            adapter_config_files.append(p)
        
        # add config file given by 'config_path'
        if config_path:
            config_path_root, config_path_ext = os.path.splitext(config_path)
            defaults_config_path = f'{config_path_root}.defaults{config_path_ext}'
            if os.path.isfile(defaults_config_path):
                adapter_config_files.append(defaults_config_path)
            if os.path.isfile(config_path):
                adapter_config_files.append(config_path)
        
        self.config = TrpConfig({
            'files': adapter_config_files
        })


    def adapter_name(self) -> str:
        """Adapter name
        Overwrite this method in your adapter to
        return the name of your adapter.
        The returned name will be used to load
        the adapter's config.
        """
        import inspect
        caller = inspect.getouterframes(inspect.currentframe())[1][3]
        raise NotImplementedError(caller + ' must be implemented in subclass')


    def capabilities(self) -> dict:
        """Capabilities of the adapter
        Overwrite this method in your adapter to make
        the capabilities of your adapter known to the
        connector.
        The return value should be a dictionary with
        the following structure:
        {
            "capability_name": {
                "description": "A short description of the capability",
                "parameters": {
                    "parameter_name": {
                        "description": "A short description of the parameter",
                        "type": "string",
                        "required": true
                    }
                }
            }
        }
        The 'parameters' key is optional. It can be used to
        describe additional parameters needed for the
        adapter to work properly.
        """
        return {}

