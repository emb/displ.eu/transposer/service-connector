#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ========================================================================================
# 
# Copyright 2023 Oliver Maklott<tinker@belowtoxic.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, 
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit 
# persons to whom the Software is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.
# 
# ========================================================================================
# Connector between the Transposer Pipeline and a service

import os
import sys
import types
import typing
import shutil
import importlib

import logging
import getopt
import time
import datetime
import uuid
import requests
import concurrent.futures

import re
import json

import signal

import copy
from deepmerge import always_merger
import dateutil

from confluent_kafka import (
    Producer,
    Consumer,
    KafkaError,
    KafkaException,
    Message,
    TopicPartition
)
from confluent_kafka.admin import (
    AdminClient,
    NewTopic
)
import socket

import paramiko
from scp import SCPClient

from cerberus import Validator

import trp_config

from utils import (
    RE_TRUE,
    RE_FALSE,
    RE_NONE,
    RE_NEGATED,
    RE_NUMBER,
    RE_INT,
    RE_FLOAT,
    RE_PRIMITIVE,
    RE_FRACTION,
    RE_SPLIT_FRACTION,
    RE_JSON_OBJECT_START,
    RE_JSON_OBJECT_END,
    RE_JSON_ARRAY_START,
    RE_JSON_ARRAY_END,
    RE_COMMA_SEPARATOR,
    RE_COMMA_TRAILING,
    RE_NEWLINE_SINGLE,
    RE_NEWLINE_MULTI,
    RE_WHITESPACE,
    RE_WHITESPACE_START,
    RE_WHITESPACE_END,
    RE_STARTSWITH_SLASH,
    RE_STARTSWITH_TILDE,
    
    get_uuid
)


# ToDo:
#   - Evaluate metrics and implement them
#   - Implement monitoring
#   - Implement error handling when unable to connect to kafka!!


ANNOUNCEMENT_TOPIC = 'transposer.announce'


class ServiceConnector:
    __slots__ = [
        'config',

        'client_id',
        'group_id',
        'private_client_id',
        'private_group_id',
        'private_producer_topic',

        'admin',
        'producer',
        'consumer',

        'consumer_topics',
        'producer_topics',
        'topic_creation_additional_confirmation_attempts',
        'topic_creation_wait_time_for_confirmation',
        'logging_topic',
        'status_topics',
        'num_partitions',
        'replication_factor',

        'log_tracebacks',
        
        'local_dir',
        'running',

        'database',

        '_adapter',
        '_adapter_instance',
        '_min_commit_count',

        '_ssh_key',
        '_ssh_key_file',
        '_ssh_host',
        '_ssh_port',
        '_ssh_user',
        '_has_ssh',
        
        '_topic_schema',
        '_topic_schema_validator',
        '_custom_topic_schema',
        '_custom_topic_schema_validator'
    ]
        
    def __init__(self, config: trp_config.TrpConfig) -> None:
        
        logging.info('Welcome to Transposer Pipeline Connector')
        
        # Main configuration
        self.config = config
        
        #print('connector config:', self.config)
        
        # Load database abstractions
        self.database = {}
        if self.config.has('database'):
            db_types = self.config.get('database').get(
                'types',
                ['sqlite', 'mysql', 'postgre', 'mongo', 'clickhouse']
            )
            for db_type in db_types:
                if not self.config.has(db_type):
                    continue
                db_opts = self.config.get(db_type).options
                if 'enable' not in db_opts or not db_opts['enable']:
                    continue
                self.database[db_type] = self._load_database(db_type)

        if not (
            self.config.has('adapter') and
            self.config.get('adapter').has('name')
        ):
            logging.critical('No adapter defined')
            print('No adapter defined')
            sys.exit(1)
        
        self.log_tracebacks = self.config.get('logging').get('tracebacks', False)
        
        self._adapter = self._load_adapter(
            self.config.get('adapter').get('name')
        )
        if self._adapter is None:
            print(f"Adapter not found: {self.config.get('adapter').get('name')}")
            sys.exit(1)
        
        self.client_id = str(uuid.uuid4())
        self.group_id = str(uuid.uuid4())
        self.private_client_id = self.client_id
        self.private_group_id = self.group_id
        self.private_producer_topic = None
        
        self._min_commit_count = self.config.get('connector').get('min.commit.count', 100)
        
        self.logging_topic = self.config.get('logging').get('topic')
        self.status_topics = {
            'INFO': self.config.get('statusTopics').get('info'),
            'WARNING': self.config.get('statusTopics').get('warning'),
            'ERROR': self.config.get('statusTopics').get('error'),
            'CRITICAL': self.config.get('statusTopics').get('critical')
        }
        
        self._ssh_key_file = self.config.get('remote').get('ssh_key_file')
        self._ssh_key = None
        if self._ssh_key_file:
            try:
                self._ssh_key = paramiko.RSAKey.from_private_key_file(self._ssh_key_file)
            except:
                pass
        self._ssh_host = self.config.get('remote').get('ssh_host')
        self._ssh_port = self.config.get('remote').get('ssh_port', 22)
        self._ssh_user = self.config.get('remote').get('ssh_user')
        self._has_ssh = True if self._ssh_key and self._ssh_host and self._ssh_user else False
        
        self.local_dir = self.config.get('storage').get('local_dir',
            self.config.get('appdirs').get('tmp')
        )
        
        self._topic_schema = {
            'topic': {'type': 'string', 'required': True},
            'partition': {'type': 'integer', 'coerce': int, 'default': -1},
            'num_partitions': {'type': 'integer', 'coerce': int, 'default': 1},
            'replication_factor': {'type': 'integer', 'coerce': int, 'default': 1}
        }
        self._topic_schema_validator = Validator(self._topic_schema)
        
        self._custom_topic_schema = {
            'topic': {'type': 'string', 'required': True},
            'partition': {'type': 'integer', 'coerce': int, 'default': -1}
        }
        self._custom_topic_schema_validator = Validator(self._topic_schema)
        
        self.running = False
        
        print('Adapter:', self._adapter)



    def run(self):
        """Run application
        
            Arguments:
                None
            
            Returns:
                None
        """
        # If there is no adapter, leave
        if self._adapter is None:
            print('Adapter not found')
            sys.exit(1)
        
        # Attach handler for signals
        signal.signal(signal.SIGINT, self._signal_handler)
        signal.signal(signal.SIGTERM, self._signal_handler)
        
        print('Run application ...')
        print('Connector config:', self.config)
        
        # Instantiate adapter
        self._adapter_instance = self._adapter.Adapter(
            self,
            self.config.get('adapter').get('config')
        )
        
        print('Adapter Instance:', self._adapter_instance)

        # Create AdminClient config
        admin_config = always_merger.merge({

            },
            self.config.get('kafkaConnectCommon').options if self.config.has('kafkaConnectCommon') else {}
        )

        # Create Producer config
        producer_config = always_merger.merge(
                always_merger.merge({
                    'client.id': self.client_id,
                    #'key_serializer': str.encode,
                    #'value_serializer': lambda v: json.dumps(v).encode('utf-8')
                    'error_cb': self._producer_error_cb
                },
                self.config.get('kafkaConnectCommon').options if self.config.has('kafkaConnectCommon') else {}
            ),
            self.config.get('kafkaConnectProducer').options if self.config.has('kafkaConnectProducer') else {}
        )
        self.client_id = producer_config['client.id']
        
        # Create Consumer config
        consumer_config = always_merger.merge(
                always_merger.merge({
                    'client.id': self.client_id,
                    'group.id': self.group_id,
                    'default.topic.config': {
                        'auto.offset.reset': self.config.get('kafkaConnectProducer').get('auto.offset.reset', 'latest')
                    },
                    'on_commit': self._commit_completed,
                    'error_cb': self._consumer_error_cb
                },
                self.config.get('kafkaConnectCommon').options if self.config.has('kafkaConnectCommon') else {}
            ),
            self.config.get('kafkaConnectConsumer').options if self.config.has('kafkaConnectConsumer') else {}
        )
        self.client_id = consumer_config['client.id']
        self.group_id = consumer_config['group.id']
        
        # Create AdminClient, Producer and Consumer
        self.admin = AdminClient(admin_config)
        self.producer = Producer(producer_config)
        self.consumer = Consumer(consumer_config)
        
        # Prepare consumer topics
        self.num_partitions = self.config.get('connector').get('num.partitions', 1)
        self.replication_factor = self.config.get('connector').get('replication.factor', 1)
        self.consumer_topics = self.config.get('connector').get('consumer.topic')
        self.topic_creation_additional_confirmation_attempts = self.config.get('connector').get('topic_creation_additional_confirmation_attempts', 3)
        self.topic_creation_wait_time_for_confirmation = self.config.get('connector').get('topic_creation_wait_time_for_confirmation', 0.01) # 0.01 is the perfect default!
        if self.consumer_topics is None:
            self.consumer_topics = []
        elif isinstance(self.consumer_topics, str):
            self.consumer_topics = [{
                'topic': self.consumer_topics,
                'partition': 0,
                'num_partitions': self.num_partitions,
                'replication_factor': self.replication_factor
            }]
        
        for i in range(len(self.consumer_topics)):
            topic = self.consumer_topics[i]
            if isinstance(topic, dict):
                self.consumer_topics[i] = self._topic_schema_validator.normalized(topic)
                continue
            self.consumer_topics[i] = {
                'topic': topic,
                'partition': 0,
                'num_partitions': self.num_partitions,
                'replication_factor': self.replication_factor
            }
        
        self.consumer_topics.append({
            'topic': self.client_id,
            'partition': 0,
            'num_partitions': 1,
            'replication_factor': 1
        })
        self.consumer_topics.append({
            'topic': self.group_id,
            'partition': 0,
            'num_partitions': self.num_partitions,
            'replication_factor': self.replication_factor
        })
        
        # Prepare producer topics
        self.producer_topics = self.config.get('connector').get('producer.topic')
        if self.producer_topics is None:
            self.private_producer_topic = f'{self.client_id}.out'
            self.producer_topics = [{
                'topic': self.private_producer_topic,
                'partition': None,
                'num_partitions': 1,
                'replication_factor': 1
            }]
        elif isinstance(self.producer_topics, str):
            self.producer_topics = [{
                'topic': self.producer_topics,
                'partition': None,
                'num_partitions': self.num_partitions,
                'replication_factor': self.replication_factor
            }]
        
        for i in range(len(self.producer_topics)):
            topic = self.producer_topics[i]
            if isinstance(topic, dict):
                self.producer_topics[i] = self._topic_schema_validator.normalized(topic)
                continue
            self.producer_topics[i] = {
                'topic': topic,
                'partition': None,
                'num_partitions': self.num_partitions,
                'replication_factor': self.replication_factor
            }
        
        try:
            # create missing topics
            if not self.createTopics(
                self.consumer_topics +
                self.producer_topics +
                [{
                    'topic': ANNOUNCEMENT_TOPIC,
                    'partition': None,
                    'num_partitions': 1,
                    'replication_factor': 1
                }]
            ):
                logging.error('Unable to create topics when starting connector')
                logging.info('Bye!')
                sys.exit(1)
            
            # Subscribe to topics
            logging.info('Subscribing to topics:')
            str_topics = []
            part_topics = []
            for topic in self.consumer_topics:
                logging.info(f"        {topic['topic']}")
                str_topics.append(topic['topic'])
                if (
                    'partition' in topic and
                    topic['partition'] is not None and
                    topic['partition'] > -1
                ):
                    part_topics.append(TopicPartition(topic['topic'], topic['partition']))
            self.consumer.subscribe(str_topics)
            # Assign partitions
            if len(part_topics) > 0:
                self.consumer.assign(part_topics)
            logging.info('    done')

            # Send announcement
            self.sendAnnouncement({
                'client.id': self.client_id,
                'group.id': self.group_id,
                'status': 200,
                'level': 'INFO',
                'message': 'Connector started',
                'state': 'running',
                'topics': {
                    'consumer': self.consumer_topics,
                    'producer': self.producer_topics
                },
                'capabilities': self._adapter_instance.capabilities()
            })
            
            # Start consumer
            logging.info('Start consumer')
            msg_count = self._min_commit_count
            self.running = True
            while self.running:
                msg = self.consumer.poll(timeout=1.0)
                
                if msg is None: continue

                if msg.error():
                    if msg.error().code() == KafkaError._PARTITION_EOF:
                        # End of partition event
                        logging.error('%% %s [%d] reached end at offset %d' %
                                        (msg.topic(), msg.partition(), msg.offset()))
                    elif msg.error():
                        logging.critical(f'Kafka Exception: {str(msg.error())}')
                        raise KafkaException(msg.error())
                else:
                    self._process_msg(msg)
                    msg_count -= 1
                    if msg_count == 0:
                        msg_count = self._min_commit_count
                        self.consumer.commit(asynchronous=True)
        finally:
            print('Shutting down ...')
            
            # We try to commit one last time
            # before the application exits.
            # That might fail if the consumer
            # never got a message. At this point
            # we don't care about the result.
            try:
                # Synchronously commit pending offsets
                logging.info('Commit offsets ...')
                self.consumer.commit(asynchronous=False)
            except:
                pass
            finally:
                logging.info('    done')
            
            logging.info('Unsubscribe ...')
            self.consumer.unsubscribe()
            time.sleep(3)
            logging.info('    done')
            
            if self.config.get('connector').get('persist.private', False):
                # Persist private topics
                logging.info('Private topics are marked as persistent. Skipping deletion.')
            else:
                # Remove private topics
                logging.info('Removing private topics ...')
                if self.private_client_id == self.client_id:
                    logging.info(f'        {self.client_id}')
                    self.removeTopics(self.client_id)
                if self.private_group_id == self.group_id:
                    logging.info(f'        {self.group_id}')
                    self.removeTopics(self.group_id)
                if self.private_producer_topic:
                    logging.info(f'        {self.private_producer_topic}')
                    self.removeTopics(self.private_producer_topic)
            
                time.sleep(2)
                logging.info('    done')
            
            # Close down consumer to commit final offsets.
            logging.info('Closing consumer ...')
            self.consumer.close()
            logging.info('    done')
            
            # Send announcement
            logging.info('Announce state ...')
            self.sendAnnouncement({
                'client.id': self.client_id,
                'group.id': self.group_id,
                'status': 200,
                'level': 'INFO',
                'message': 'Connector stopped',
                'state': 'stopped'
            })
            logging.info('    done')
            
            # Flush producer to deliver messages still in queue.
            logging.info('Flushing producer ...')
            self.producer.flush(60)
            logging.info('    done')

            logging.info('Bye!')
            print('Bye!')


    def stop(self):
        """Stop application
        
            Arguments:
                None
            
            Returns:
                None
        """
        self.running = False



    ### Consumer handling
    
    def _commit_completed(self, err: Exception, partitions: list) -> None:
        """Commit completed callback
        
            Arguments:
                err {Exception} -- Exception object
                partitions {list} -- List of partitions
            
            Returns:
                None
        """
        if err:
            logging.error(f'Commit failed for partitions: {str(err)}')
        else:
            logging.info(f'Commit completed for partitions: {str(partitions)}')


    def _process_msg(self, msg: Message) -> None:
        """Process message
        
            Arguments:
                msg {Message} -- Kafka message
            
            Returns:
                None
        """
        topic = None
        try:
            topic = msg.topic()
        except:
            pass
        
        payload = None
        try:
            payload = msg.value()
        except:
            pass
        
        try:
            payload = payload.decode('utf-8')
        except:
            pass
            
        if isinstance(payload, str):
            try:
                payload = json.loads(payload)
            except:
                pass
        
        #self._current_payload = payload
        
        result = self._adapter_instance.run(payload, msg)
        
        print('Adapter result:', result)
        
        # Evaluate status
        status_type = 'ERROR'
        status_num = 500
        status_message = None
        if 'success' in result and result['success']:
            status_type = 'INFO'
            status_num = 200
        if 'status' in result:
            status_num = result['status']
            status_type = self.config.get('logStatusMapping').get(str(status_num), 'ERROR')
        if 'message' in result and result['message']:
            status_message = result['message']
        else:
            status_message = self.config.get('statusMapping').get(str(status_num))
        status_type_lc = status_type.lower()
        status_type_uc = status_type.upper()
        
        # Fill in the gaps
        result['status'] = status_num
        result['level'] = status_type_uc
        result['message'] = status_message
        if 'success' not in result:
            result['success'] = status_num < 400
        
        result['client.id'] = self.client_id
        result['group.id'] = self.group_id
        
        # Log status
        log_f = getattr(logging, status_type_lc)
        if callable(log_f):
            if status_message:
                log_f(status_message)
            else:
                log_f(status_num)
            
        # Send log
        self.sendLog(result)
        
        # Send status
        self.sendStatus(result)
            
        # Send response
        if 'topic' in result:
            self.sendResponseTo(result)
        else:
            self.sendResponse(result)


    def _producer_error_cb(self, err: Exception) -> None:
        """Kafka Producer error callback
        
            Arguments:
                err {Exception} -- Exception object
            
            Returns:
                None
        """
        logging.error(f'Kafka Producer error: {str(err)}')
        if self.log_tracebacks:
            logging.exception(err)


    def _consumer_error_cb(self, err: Exception) -> None:
        """Kafka Consumer error callback
        
            Arguments:
                err {Exception} -- Exception object
            
            Returns:
                None
        """
        logging.error(f'Kafka Consumer error: {str(err)}')
        if self.log_tracebacks:
            logging.exception(err)



    ### Message handling

    # Send log message to kafka
    def sendLog(self, data: dict | None = None) -> None:
        """Send log message to kafka
        
            Arguments:
                data {dict} -- Data object
            
            Returns:
                None
        """
        if not isinstance(data, dict):
            return
        
        if self.logging_topic is None:
            return

        if getattr(logging, data['level'], 10) < logging.root.level:
            return
        
        try:
            self.producer.produce(
                self.logging_topic,
                key=str(uuid.uuid4()),
                value=json.dumps(data)
            )
        except Exception as err:
            logging.error(f'Unable to send log message: {str(err)}')
            if self.log_tracebacks:
                logging.exception(err)



    # Send status message to kafka
    def sendStatus(self, data: dict | None = None) -> None:
        """Send status message to kafka
        
            Arguments:
                data {dict} -- Data object
            
            Returns:
                None
        """
        if not isinstance(data, dict):
            return
        
        if (
            data['level'] not in self.status_topics or
            not self.status_topics[data['level']]
        ):
            return

        try:
            self.producer.produce(
                self.logging_topic,
                key=str(uuid.uuid4()),
                value=json.dumps(data)
            )
        except Exception as err:
            logging.error(f'Unable to send status message: {str(err)}')
            if self.log_tracebacks:
                logging.exception(err)



    # Send response message to kafka
    def sendResponse(self, data: dict | None = None) -> None:
        """Send response message to kafka
        
            Arguments:
                data {dict} -- Data object
            
            Returns:
                None
        """
        if not isinstance(data, dict):
            return

        print('sendResponse ... data:', data)
        
        try:
            data = json.dumps(data)
            uid = str(uuid.uuid4())
            for topic in self.producer_topics:
                if (
                    'partition' in topic and
                    topic['partition'] is not None and
                    topic['partition'] > -1
                ):
                    self.producer.produce(
                        topic['topic'],
                        value=data,
                        key=uid,
                        partition=topic['partition']
                    )
                    continue
                self.producer.produce(
                    topic['topic'],
                    value=data,
                    key=uid
                )
        except Exception as err:
            logging.error(f'Unable to send message: {str(err)}')
            if self.log_tracebacks:
                logging.exception(err)



    # Send response message to kafka on custom topics
    def sendResponseTo(self, data: dict | None = None) -> None:
        """Send response message to kafka on custom topics
        
            Arguments:
                data {dict} -- Data object
            
            Returns:
                None
        """
        if not isinstance(data, dict):
            return
        
        topics = data['topic']
        if isinstance(topics, str):
            topics = [{'topic': topics}]
        elif isinstance(topics, dict):
            topics = [topics]
        
        for i in range(len(topics)):
            topic = topics[i]
            if isinstance(topic, dict):
                topics[i] = self._custom_topic_schema_validator.normalized(topic)
                continue
            topics[i] = {
                'topic': topic,
                'partition': None
            }
        
        #print('sendResponseTo ... data:', data)
        
        data = data.copy()
        del data['topic']
        
        try:
            data = json.dumps(data)
            uid = str(uuid.uuid4())
            for topic in topics:
                if (
                    'partition' in topic and
                    topic['partition'] is not None and
                    topic['partition'] > -1
                ):
                    self.producer.produce(
                        topic['topic'],
                        value=data,
                        key=uid,
                        partition=topic['partition']
                    )
                    continue
                self.producer.produce(
                    topic['topic'],
                    value=data,
                    key=uid
                )
        except Exception as err:
            logging.error(f'Unable to send message: {str(err)}')
            if self.log_tracebacks:
                logging.exception(err)



    # Send announcement to kafka
    def sendAnnouncement(self, data: dict | None = None) -> None:
        """Send announcement message to kafka
        
            Arguments:
                data {dict} -- Data object
            
            Returns:
                None
        """
        if not isinstance(data, dict):
            return
        
        try:
            self.producer.produce(
                ANNOUNCEMENT_TOPIC,
                key=str(uuid.uuid4()),
                value=json.dumps(data)
            )
        except Exception as err:
            logging.error(f'Unable to send announcement message: {str(err)}')
            if self.log_tracebacks:
                logging.exception(err)



    # Create Kafka topics
    def createTopics(self, topics: str | list) -> bool:
        """Create Kafka topics.

        Arguments:
            topics {str | list} -- Name or list of the topics to create.

        Returns:
            None
        """
        if isinstance(topics, str):
            topics = [{
                'topic': topics,
                'partition': 0,
                'num_partitions': self.num_partitions,
                'replication_factor': self.replication_factor
            }]

        # Get the list of existing topics
        existing_topics = self.admin.list_topics().topics.keys()

        # Filter topics for existing and non-existing topics
        topics = [x for x in topics if x['topic'] not in existing_topics]
        
        # Leave if there are no topics to create
        if not topics:
            return True
        
        # Create NewTopic instances with the topics to create
        
        #print('createTopics ...')
        #for t in topics:
        #    print('    t:', t)
        
        new_topics = [NewTopic(
            x['topic'],
            num_partitions=x['num_partitions'],
            replication_factor=x['replication_factor']
        ) for x in topics]

        logging.info(f'Creating topics:')
        for topic in topics:
            logging.info(f"        {topic['topic']}")

        additional_confirmation_attempts = self.topic_creation_additional_confirmation_attempts
        wait_time = self.topic_creation_wait_time_for_confirmation
        try:
            result = self.admin.create_topics(new_topics)
            # Wait for operation to finish.
            result = concurrent.futures.wait(list(result.values()), timeout=60)
                    
            if self._confirm_topics_creation(new_topics, additional_confirmation_attempts, wait_time):
                logging.info('Topics created successfully')
                return True
            return False
        except Exception as e:
            logging.error(f'Error creating topics: {e}')
            return False

    def _confirm_topics_creation(self, topics, additional_confirmation_attempts, wait_time):
        logging.info(f"Confirming topic creation (up to {additional_confirmation_attempts + 1} attempts)...")
        
        # First attempt
        if self._check_missing_topics(topics):
            return True
        
        logging.info("Initial creation attempt completed, but the creation of some topics was not confirmed...")
        
        # Additional attempts
        for attempt in range(additional_confirmation_attempts):
            logging.info(f"Confirmation attempt {attempt + 2} of {additional_confirmation_attempts + 1}, checking again in {wait_time} seconds...")
            time.sleep(wait_time)
            
            if self._check_missing_topics(topics):
                return True
        
        logging.error(f"Some topics were not confirmed after {additional_confirmation_attempts + 1} attempts")
        return False

    def _check_missing_topics(self, topics):
        topic_metadata = self.admin.list_topics()
        missing_topics = [topic.topic for topic in topics if topic.topic not in topic_metadata.topics]
        
        if not missing_topics:
            logging.info("All topics have been confirmed as created")
            return True
        
        missing_topics_str = ", ".join(missing_topics)
        logging.info(f"Waiting for the creation confirmation of the following topics: {missing_topics_str}")
        return False

    # Remove Kafka topics
    def removeTopics(self, topics: str | list) -> bool:
        """Remove Kafka topics.

        Arguments:
            topics {str | list} -- Name or list of the topics to remove.

        Returns:
            None
        """
        if isinstance(topics, str):
            topics = [{'topic': topics}]

        str_topics = []
        for topic in topics:
            if isinstance(topic, dict):
                str_topics.append(topic['topic'])
                continue
            if isinstance(topic, str):
                str_topics.append(topic)

        # Call the delete_topics() method with the NewTopic instance
        result = self.admin.delete_topics(str_topics)
        
        # Wait for operation to finish.
        result = concurrent.futures.wait(list(result.values()), timeout=60)

        # Return success
        if len(result.not_done) == 0:
            return True
        return False

    



    ### Adapter handling
    
    # load adapter
    def _load_adapter(self, name: str | None = None) -> types.ModuleType | None:
        """Load adapter by name.
            
            Arguments:
                name {str} -- Name of adapter
            
            Returns:
                Adapter | None
        """
        if not name:
            return None
        try:
            return importlib.import_module(name)
        except Exception as err:
            logging.critical(f'Unable to load adapter: {str(err)}')
            if self.log_tracebacks:
                logging.exception(err)
            return None



    # load database
    def _load_database(self, name: str | None = None) -> types.ModuleType | None:
        """Load database by name.
            
            Arguments:
                name {str} -- Name of database
            
            Returns:
                Database module | None
        """
        if not name:
            return None
        try:
            return importlib.import_module(f'database.{name}')
        except Exception as err:
            logging.critical(f'Unable to load database: {str(err)}')
            if self.log_tracebacks:
                logging.exception(err)
            return None



    ### File handling
    
    # Get file from remote machine via SCP
    def getRemoteFile(self, src, trg: str | None = None) -> str | None:
        """Get file from remote machine via SCP.
        
            Arguments:
                src {str} -- Source file
                trg {str | None} -- Target file
            
            Returns:
                str | None -- Path to file
        """
        with paramiko.SSHClient() as ssh:
            try:
                #ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
                ssh.load_system_host_keys()
                ssh.connect(
                    self._ssh_host,
                    username=self._ssh_user,
                    pkey=self._ssh_key
                )
                
                filename = os.path.basename(src)
                if trg:
                    if RE_STARTSWITH_SLASH.match(trg):
                        if os.path.isdir(trg):
                            trg = os.path.join(trg, filename)
                    elif RE_STARTSWITH_TILDE.match(trg):
                        trg = os.path.expanduser(trg)
                        if os.path.isdir(trg):
                            trg = os.path.join(trg, filename)
                    else:
                        trg = os.path.join(self.local_dir, trg)
                        if os.path.isdir(trg):
                            trg = os.path.join(trg, filename)
                else:
                    trg = os.path.join(self.local_dir, filename)
                
                with SCPClient(ssh.get_transport()) as scp:
                    scp.get(src, trg)
                    
            except:
                return None

        return trg


    # Put file to remote machine via SCP
    def putRemoteFile(self, src, trg: str | None = None) -> str | None:
        """Get file from remote machine via SCP.
        
            Arguments:
                src {str} -- Source file
                trg {str | None} -- Target file
            
            Returns:
                str | None -- Path to file
        """
        with paramiko.SSHClient() as ssh:
            try:
                #ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
                ssh.load_system_host_keys()
                ssh.connect(
                    self._ssh_host,
                    username=self._ssh_user,
                    pkey=self._ssh_key
                )
                
                filename = os.path.basename(src)
                if not trg:
                    trg = filename
                
                with SCPClient(ssh.get_transport()) as scp:
                    scp.put(src, trg)
                    
            except:
                return None

        return trg

    
    # Fetch file from URL
    def getUrl(self, url: str, trg: str | None = None) -> str | None:
        """Get file from URL.

            Arguments:
                url {str} -- URL of file
                trg {str | None} -- Target file

            Returns:
                str | None -- Path to file
        """
        # Download file from URL
        response = requests.get(url)
        if response.status_code != 200:
            return None

        # Save file to local path
        filename = os.path.basename(url)
        if trg:
            if RE_STARTSWITH_SLASH.match(trg):
                if os.path.isdir(trg):
                    trg = os.path.join(trg, filename)
            elif RE_STARTSWITH_TILDE.match(trg):
                trg = os.path.expanduser(trg)
                if os.path.isdir(trg):
                    trg = os.path.join(trg, filename)
            else:
                trg = os.path.join(self.local_dir, trg)
                if os.path.isdir(trg):
                    trg = os.path.join(trg, filename)
        else:
            trg = os.path.join(self.local_dir, filename)
        
        with open(trg, 'wb') as fh:
            fh.write(response.content)

        return trg
    

    # Copy local file
    def copyLocalFile(self, src: str, trg: str | None = None) -> str | None:
        """Copy local file.
        
            Arguments:
                src {str} -- Source file
                trg {str | None} -- Target file
            
            Returns:
                str | None -- Path to file
        """
        filename = os.path.basename(src)
        if trg:
            if RE_STARTSWITH_SLASH.match(trg):
                if os.path.isdir(trg):
                    trg = os.path.join(trg, filename)
            elif RE_STARTSWITH_TILDE.match(trg):
                trg = os.path.expanduser(trg)
                if os.path.isdir(trg):
                    trg = os.path.join(trg, filename)
            else:
                trg = os.path.join(self.local_dir, trg)
                if os.path.isdir(trg):
                    trg = os.path.join(trg, filename)
        else:
            trg = os.path.join(self.local_dir, filename)
        
        try:
            return shutil.copyfile(src, trg)
        except:
            return None
    
    
    
    # Remove local file(s)
    def removeLocalFiles(self, src: str | list) -> None:
        """Remove file(s).
        
            Arguments:
                src {str | list} -- Source file(s)
                
            Returns:
                None
        """
        if isinstance(src, str):
            src = [src]
        for file in src:
            if os.path.isfile(file):
                try:
                    os.remove(file)
                except:
                    pass
    
    
    
    

    ### Signal handling and cleanup
    
    # signal handler
    def _signal_handler(self, signum, frame):
        logging.info(f'Caught signal: {signum}')
        self._cleanup()
    
    # cleanup
    def _cleanup(self):
        logging.info('Stopping connector ...')
        self.running = False






###############################################################
# DEBUG / TESTING
#
if __name__ == '__main__':
    pass

