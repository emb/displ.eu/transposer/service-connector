#!/bin/bash

# Start the Service Connector.
# This script changes into the project
# directory /opt/transposer/service-connector,
# starts the python virtualenv and the
# service-connector app.

cd /opt/transposer/service-connector
source bin/activate
app/connector "$@"
